import caffe_pb2
import lmdb
import datetime
import time
import os
import csv
import random
import sys
from collections import OrderedDict, Iterable

import numpy as np

import keras
from keras import backend as K
from keras import initializations
from keras.models import Model
from keras.layers import Input, Layer, Convolution2D, Flatten, Dropout
from keras.layers.core import Activation
from keras.optimizers import SGD
from keras.utils.np_utils import to_categorical
from keras.callbacks import Callback
from keras.regularizers import l2
from keras.constraints import maxnorm

class Biasadd(Layer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        
    def build(self, input_shape):
        bias = np.zeros((input_shape[1], input_shape[2], input_shape[3]))
        self.bias = K.variable(value = bias, name="bias")
        self.trainable_weights = [self.bias]
        self.built = True
        
    def get_output_shape_for(self, input_shape):
        return input_shape
        
    def call(self, x, mask = None):
        return x + self.bias

class Convolution2DLocalBias():
    def __init__(self, depth, size_x, size_y, name, **kwargs):
        self.depth = depth
        self.size_x = size_x
        self.size_y = size_y
        self.name = name
        self.activation = kwargs.pop("activation", "")
        self.kwargs = kwargs
    def __call__(self, inpt):
        # Convolution 
        self.conv = Convolution2D(self.depth, self.size_x, self.size_y, name = self.name + "_conv", **self.kwargs)(inpt)
        # Add a bias individually to all responses
        self.bias = Biasadd(name = self.name + "_bias")(self.conv)
        # Then do the nonlinear activation
        if self.activation != "":
            self.act = Activation(activation=self.activation, name = self.name + "_" + self.activation)(self.bias)
            return self.act
        else:
            return self.bias

class MyCallback(Callback):
    def __init__(self, begin_lr, halve_lr_iter, print_iter, test_iter, total_iter, x_test, y_test, x_train, y_train, name_of_train, logfile):
        super(MyCallback, self).__init__()
        self.lr = begin_lr
        self.halve_iter = halve_lr_iter
        self.left_halve = halve_lr_iter
        self.print_iter = print_iter
        self.left_print = print_iter
        self.test_iter  = test_iter
        self.left_test  = 0 #test at start too
        self.total_iter = total_iter
        self.x_test     = x_test
        self.y_test     = y_test
        self.x_train    = x_train
        self.y_train    = y_train
        self.name_of_train = name_of_train
        self.logfile       = logfile

    def on_train_begin(self, logs={}):
        self.current_iter = 0
        self.nb_epoch = self.params['nb_epoch']
        self.start_time = time.time()
        self.outfile = open(self.logfile, "w")
        self.best_test_acc = 0
    def on_train_end(self, logs={}):
        train_time = time.time() - self.start_time
        print(datetime.datetime.now().strftime("%H:%M:%S") + ' Total train time was: ' + str(datetime.timedelta(seconds=int(train_time))))
        self.outfile.close()
    def on_epoch_begin(self, epoch, logs={}):
        print(datetime.datetime.now().strftime("%H:%M:%S") + ' Begin Epoch %d/%d' % (epoch + 1, self.nb_epoch))
    def on_batch_end(self, batch, logs={}):
        self.current_iter += 1
        self.left_halve -= 1
        self.left_print -= 1
        self.left_test  -= 1
        if self.left_halve <= 0:
            self.left_halve = self.halve_iter
            self.lr = self.lr * 0.5
            print(datetime.datetime.now().strftime("%H:%M:%S") + " Updating learning rate to ", self.lr)
            K.set_value(self.model.optimizer.lr, self.lr)
        if self.left_print <= 0:
            self.left_print = self.print_iter
            elapsed_time = time.time() - self.start_time
            done_part = self.current_iter / self.total_iter
            estimated_total_time = elapsed_time / done_part
            eta_left = str(datetime.timedelta(seconds=int(estimated_total_time - elapsed_time)))
            eta = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(self.start_time + estimated_total_time))
            print(datetime.datetime.now().strftime("%H:%M:%S") + " [" + self.name_of_train + "] Currently at iter %8d/%d, (%.2f%%, eta " % (self.current_iter, self.total_iter, 100.0*self.current_iter / self.total_iter) + eta_left + " [" + eta + "] )")
        if self.left_test <= 0:
            self.left_test = self.test_iter + self.left_test
            scores = self.model.evaluate(self.x_test, self.y_test, batch_size=128, verbose=0)
            scores_train = self.model.evaluate(self.x_train, self.y_train, batch_size=128, verbose=0)
            print(datetime.datetime.now().strftime("%H:%M:%S") + " Testing at iter %8d, acc=%.2f%%, val_acc=%.2f%%" % (self.current_iter, scores_train[1]*100.0, scores[1]*100.0))
            self.outfile.write(str(self.current_iter) + " " + str(scores_train[1]) + " " + str(scores_train[0]) + " " + str(scores[1]) + " " + str(scores[0]) + "\n")
            self.outfile.flush()
            os.fsync(self.outfile)
            if self.best_test_acc < scores[1]:
                self.best_test_acc = scores[1]
                self.model.save('models/model_' + self.name_of_train)
     

def load_data(path, filter_planes, number_random_samples = 0):
    lmdb_env = lmdb.open(path)
    lmdb_txn = lmdb_env.begin()
    lmdb_cursor = lmdb_txn.cursor()
    datum = caffe_pb2.Datum()

    nbsamples = 0
    nblayers = 0
    height = 0
    width = 0
    x = []
    y = []
    #limit = 30
    for key, value in lmdb_cursor:
        #if nbsamples == limit:
        #    break
        nbsamples += 1
        nblayers = datum.channels
        height = datum.height
        width = datum.width
        datum.ParseFromString(value)
        new_x = np.ndarray(shape=(datum.channels, datum.height, datum.width), buffer=np.fromstring(datum.data, dtype=np.uint8), dtype=np.uint8)
        number_filtered_layers = len(filter_planes[filter_planes == 1])
        if  number_filtered_layers > 0: #if there is something to filter
            nblayers -= number_filtered_layers
            new_x = new_x[filter_planes == 0]

        x.append(new_x)
        y.append(datum.label)

    x_random = []
    y_random = []
    already_picked = set()
    while len(x_random) < number_random_samples and len(x_random) < nbsamples/2:
        pick = random.randint(0, nbsamples - 1)
        if not (pick in already_picked):
            x_random.append(x[pick])
            y_random.append(y[pick])
            already_picked.add(pick)
    if number_random_samples > 0:
        return nbsamples, nblayers, height, width, np.stack(x), to_categorical(y, nb_classes=height*width), np.stack(x_random), to_categorical(y_random, nb_classes=height*width)
    return nbsamples, nblayers, height, width, np.stack(x), to_categorical(y, nb_classes=height*width)

def my_init(shape, name=None):
    #return initializations.uniform(shape, scale=0.05, name=name)
    return initializations.he_uniform(shape, name=name)

def apply_reg(d): #required because regularizers cannot be reused in multiple layers
    if "W_regularizer" in d:
        return dict(d, W_regularizer=l2(d["W_regularizer"]))
    else:
        return d

def main(number_convolution_filters = 192, number_3x3 = 10, lambda_l2 = 0.0005, activation="relu", use_pdb = True,
         filter_empty = False, filter_turns_since = False, filter_liberties = False, filter_capture_size = False,
         filter_self_atari = False, filter_liberties_after = False, filter_kgs_rank = False):
    filter_planes = np.array([0]*42) #there are 42 planes
    addon = ""
    if filter_empty:
        filter_planes[2] = 1
        addon += "empty"
    if filter_turns_since:
        for i in range(0, 5):
            filter_planes[3+i] = 1
        addon += "turnssince"
    if filter_liberties:
        for i in range(0, 4):
            filter_planes[8+i] = 1
        addon += "liberties"
    if filter_capture_size:
        for i in range(0, 7):
            filter_planes[12+i] = 1
        addon += "capturesize"
    if filter_self_atari:
        for i in range(0, 7):
            filter_planes[19+i] = 1
        addon += "selfatari"
    if filter_liberties_after:
        for i in range(0, 6):
            filter_planes[26+i] = 1
        addon += "libertiesafter"
    if filter_kgs_rank:
        for i in range(0, 9):
            filter_planes[32+i] = 1
        addon += "kgsrank"

    number_epochs = 16
    batch_size = 16
    learning_rate_start = 0.003
    halve_lr_iter = 1000000
    print_iter = 10000
    test_iter = 25000
    test_on_train_samples = 25000 #howmany training samples we pick to evaluate performance

    name_of_train = str(number_3x3)+ "_3x3"
    if use_pdb:
        name_of_train += "_pdb"
        MyConv2D = Convolution2DLocalBias
    else:
        name_of_train += "_nopdb"
        MyConv2D = Convolution2D

    name_of_train += "_" + str(number_convolution_filters) + "filters"

    if lambda_l2 != 0:
        name_of_train += "_l2reg_lambda_" + str(lambda_l2).replace(".", "_")
    else:
        name_of_train += "_noreg"

    name_of_train += "_" + activation
    name_of_train += "_he_uniform"
    if addon != "":
        name_of_train += "_filter_" + addon

    print("Training begins: " + name_of_train)
    nbsamples, nblayers, width, height, x, y, x_train, y_train = load_data("data/nn_training_data_lmdb", filter_planes, test_on_train_samples)
    print("Read training data: ", nbsamples, " Samples")
    nbsamples_test, _, _, _, x_test, y_test = load_data("data/nn_test_data_lmdb", filter_planes)
    print("Read test data: ", nbsamples_test, " Samples")

    conv_kwargs = dict(border_mode="same", init=my_init, activation=activation)
    if lambda_l2 != 0:
        conv_kwargs["W_regularizer"] = lambda_l2

    name_of_train=datetime.datetime.now().strftime("%Y_%m_%d__") + name_of_train

    # Define input shape
    inputs = Input(shape=(nblayers, width, height), name="Input") # For example each sample has 9x9 with 42 channels

    # 5x5 convolution
    step = MyConv2D(number_convolution_filters, 5, 5, name="conv5x5", **apply_reg(conv_kwargs))(inputs)

    for i in range(number_3x3):
        step = MyConv2D(number_convolution_filters, 3, 3, name="conv3x3_" + str(i), **apply_reg(conv_kwargs))(step)

    step  = MyConv2D(1, 1, 1, name="convolution_end", **apply_reg(conv_kwargs))(step)
    step = Flatten(name="flatten")(step)
    prediction = Activation("softmax")(step)

    model = Model(input=inputs, output=prediction)

    sgd = SGD(lr=learning_rate_start, decay=0, momentum=0, nesterov=False)

    model.compile(optimizer=sgd,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    # Look at summary
    model.summary()

    my_callback = MyCallback(begin_lr=learning_rate_start,
                             halve_lr_iter=halve_lr_iter,
                             print_iter=print_iter,
                             test_iter=test_iter,
                             total_iter=nbsamples/batch_size * number_epochs,
                             x_test = x_test, y_test = y_test,
                             x_train = x_train, y_train = y_train,
                             name_of_train=name_of_train,
                             logfile='logs/' + name_of_train + '.dat')

    model.fit(x, y, batch_size = batch_size, nb_epoch = number_epochs, verbose = 0, callbacks=[my_callback])

#This allows calling the code from cmdline via something like:
#python my_net.py number_convolution_filters=16 number_3x3=10 lambda_l2=0.0005 'activation="relu"' use_pdb=True
exec('main(' + ', '.join(sys.argv[1:]) + ')')
