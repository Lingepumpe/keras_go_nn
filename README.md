Neural Network learning to predict Go Moves on 9x9 boards
=========================================================

Rewrite of the Caffe version of a neural network written to
Keras. It is supposed to learn how good go players play the
game of Go on the small 9x9 board.

Scope
-----
The project is part of my bachelor thesis "Deep Neural Networks applied to the
game of Go".


