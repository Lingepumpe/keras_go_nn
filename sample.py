import numpy as np

import keras
from keras import backend as K
from keras.models import Model
from keras.layers import Input, Layer, Convolution2D
from keras.layers.core import Activation

class Biasadd(Layer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        
    def build(self, input_shape):
        bias = np.zeros((input_shape[1], input_shape[2], input_shape[3]))
        self.bias = K.variable(value = bias, name="bias")
        self.trainable_weights = [self.bias]
        self.built = True
        
    def get_output_shape_for(self, input_shape):
        return input_shape
        
    def call(self, x, mask = None):
        return x + self.bias

class Convolution2DLocalBias():
    def __init__(self, depth, size, name, **kwargs):
        self.depth = depth
        self.size = size
        self.name = name
        self.kwargs = kwargs
    def __call__(self, inpt):
        # Convolution 
        self.conv = Convolution2D(self.depth, self.size, self.size, border_mode="same", name = self.name + "_conv")(inpt)
        # Add a bias individually to all responses
        self.bias = Biasadd(name = self.name + "_bias")(self.conv)
        # Then do the nonlinear activation
        self.relu = Activation(activation="relu", name = self.name + "_relu")(self.bias)
        return self.relu


# Define input shape
inpt = Input(shape=(43,9,9), name="Input") # For example 9x9 with 43 channels

# 5x5 convolution 
conv1 = Convolution2DLocalBias(64,5,name="conv1")(inpt)
conv2 = Convolution2DLocalBias(64,3,name="conv2")(conv1)

# Define a model with the given layers
model = Model(input=inpt, output=conv2)
# Compile the model (might take some time)
model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])

# Look at summary
model.summary()
